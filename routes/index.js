let express = require("express");
let router = express.Router();
let path = require("path");
const Handlebars = require("handlebars");
const axios = require("axios");

// var resumeDetails = {};
// Handlebars.registerHelper("prod", () => {
//   console.log("came in here....header");
//   return process.env.NODE_ENV === "prod";
// });

Handlebars.registerHelper("apiBaseURL", () => {
  // if (process.env.NODE_ENV === "prod")
  //   return process.env.PROD_API_MASTERUNION_ORG;
  return process.env.DEV_API_INVESTMENT_FUND;
});

// Get Homepage
router.get("/", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("team", {
    title: "Investmentfund",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});

// $(document).on('click', 'ul li', function(){
//   $(this).addClass('active').siblings().removeClass('active')
// })


// Get about-us
router.get("/about-us", function (req, res, next) {
    //   res.render("index", { layout: "main", template: "index" });
    res.render("about-us", {
      title: "Investmentfund",
      // isProd: process.env.NODE_ENV === "prod",
      description: "",
      metaData: {
        img: "",
        url: "",
      },
    });
});

// Get about-jetbrains
router.get("/about-jetbrain", function (req, res, next) {
    //   res.render("index", { layout: "main", template: "index" });
    res.render("about-jetbrain", {
      title: "Investmentfund",
      // isProd: process.env.NODE_ENV === "prod",
      description: "",
      metaData: {
        img: "",
        url: "",
      },
    });
});

router.get("/about-jetbrains", function (req, res, next) {
    //   res.render("index", { layout: "main", template: "index" });
    res.render("about-jetbrain", {
      title: "Investmentfund",
      // isProd: process.env.NODE_ENV === "prod",
      description: "",
      metaData: {
        img: "",
        url: "",
      },
    });
});
// Get blog
router.get("/blog", function (req, res, next) {
    //   res.render("index", { layout: "main", template: "index" });
    res.render("blog", {
      title: "Investmentfund",
      // isProd: process.env.NODE_ENV === "prod",
      description: "",
      metaData: {
        img: "",
        url: "",
      },
    });
});
// Get document
router.get("/document", function (req, res, next) {
    //   res.render("index", { layout: "main", template: "index" });
    res.render("document", {
      title: "Investmentfund",
      // isProd: process.env.NODE_ENV === "prod",
      description: "",
      metaData: {
        img: "",
        url: "",
      },
    });
});
// Get investment
router.get("/investment", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("investment", {
    title: "Investmentfund",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});
// Get mentors
router.get("/mentors", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("mentors", {
    title: "Investmentfund",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});
// Get offer
router.get("/offer", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("offer", {
    title: "Investmentfund",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});
// Get portfolio
router.get("/portfolio", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("portfolio", {
    title: "Investmentfund",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});
// Get performance
router.get("/performance", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("performance", {
    title: "Investmentfund",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});
// Get team
router.get("/team", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("team", {
    title: "Investmentfund",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});
// Get contact-us
router.get("/contact-us", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("contact-us", {
    title: "Investmentfund",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});

module.exports = router;

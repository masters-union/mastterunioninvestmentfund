#!/bin/bash

# Get servers list
set -f
string=$STAGING_DEPLOY_SERVER
array=(${string//,/ })

# iterate servers for deploy and pull latest code
for i in "${!array[@]}"; do
  echo "deploying to ${array[i]}"
  ssh ubuntu@${array[i]} "cd /home/ubuntu/mastterunioninvestmentfund && git pull origin staging && npm i && forever restart investmentfund" 
done
